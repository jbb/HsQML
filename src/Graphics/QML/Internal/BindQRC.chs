-- SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
--
-- SPDX-License-Identifier: BSD-3-Clause

{-# LANGUAGE
    ForeignFunctionInterface
  #-}

module Graphics.QML.Internal.BindQRC where

import Foreign.C.Types
import Foreign.Ptr

#include "hsqml.h"

--
-- QRC
--

{#fun unsafe hsqml_register_resource_data as ^
  {fromIntegral `Int',
   id `Ptr CUChar',
   id `Ptr CUChar',
   id `Ptr CUChar'} -> 
  `CInt' #}
