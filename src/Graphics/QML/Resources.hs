-- SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
--
-- SPDX-License-Identifier: BSD-3-Clause

-- | Bundle QML and other resources used by the application
module Graphics.QML.Resources (
  includeResources,
  Resource (
    Resource,
    baseDir,
    prefix,
    files
  ),
  File (
    File,
    path,
    alias
  ),
  registerResourceData) where

import Data.Bits
import Data.Char
import Data.Int
import Data.Word
import Data.Maybe
import Data.List

import qualified Data.ByteString as BS

import Language.Haskell.TH
import Language.Haskell.TH.Syntax

import System.Environment

import Control.Monad

import Graphics.QML.Internal.BindQRC

import Data.ByteString.Internal

import Foreign.C.Types
import Foreign.ForeignPtr.Unsafe
import Foreign.ForeignPtr

--
-- Virtual filesystem tree
--

data HashedString = HashedString {
    hash :: Int,
    string :: String
} deriving (Eq, Ord)

instance Show HashedString where
    show (HashedString _ str) = show str

-- | A file in a resource
--
-- path is the path to the file on the filesystem.
--
-- alias is the path (minus the prefix) at which the file should be found at runtime.
data File = File {
    path :: String,
    alias :: Maybe String
}

instance Show File where
    show (File path (Just alias)) = path ++ " (" ++ alias ++ ")"
    show (File path Nothing) = path

data TreeNode = FileNode String | DirectoryNode {
    contents :: [(HashedString, TreeNode)],
    offset :: Int
} deriving (Show)

newDir :: TreeNode
newDir = DirectoryNode [] 0

newFile :: String -> TreeNode
newFile = FileNode

insertChild :: String -> TreeNode -> TreeNode -> TreeNode
insertChild name node (DirectoryNode c offset) = DirectoryNode (sortOn fst $ insertIntoContents name node c) offset
    where
        insertIntoContents :: String -> TreeNode -> [(HashedString, TreeNode)] -> [(HashedString, TreeNode)]
        insertIntoContents name node ((existingName, DirectoryNode c offset):xs)
            | name == string existingName = (existingName, DirectoryNode (c ++ contents node) offset) : xs
            | otherwise                   = (existingName, DirectoryNode c offset) : insertIntoContents name node xs

        insertIntoContents name node [] = [(hashString name, node)]

        -- skip file nodes, as we can't insert into them any way
        insertIntoContents name node (x:xs) = x : insertIntoContents name node xs

insertChild name node _ = error "Can't insert child node into file"

computeOffsets :: TreeNode -> Int -> TreeNode
computeOffsets (DirectoryNode contents _) offset =
    let contentLen = length contents
        childrenOffset = contentLen + offset
        newContents = map (`setChildOffset` childrenOffset) contents
    in  DirectoryNode newContents offset
    where
        setChildOffset :: (HashedString, TreeNode) -> Int -> (HashedString, TreeNode)
        setChildOffset (nameHash, DirectoryNode contents _) offset = (nameHash, computeOffsets (DirectoryNode contents offset) offset)
        setChildOffset (nameHash, file) _ = (nameHash, file)
computeOffsets file offset = file

-- | A resource represents a tree of resource files.
--
-- The base dir controls the folder to which the files are relative on the filesystem.
--
-- The prefix controls where the files will be accessible inside QML.
-- Commonly, "/" is used.
--
-- The files field contains the list of files that should be included in the prefix.
--
-- The prefix and path are combined, and at runtime the file can be found at "qrc:${PREFIX}${FILE PATH}"
data Resource = Resource {
    -- | Local file system paths in this resource are relative to this base directory.
    baseDir :: Maybe String,
    -- | Virtual file system paths in qrc are prefixed with this path.
    prefix :: String,
    -- | Vector of files inside this resource.
    -- Their aliases (virtual paths) will be prefixed by the `prefix` path,
    -- and their location on the local file system will be prefixed by `baseDir`.
    files :: [File]
} deriving (Show)

-- remove duplicate, or leading '/'
simplifyPrefix :: String -> String
simplifyPrefix xs = simplifyPrefix' xs '/'
    where
        simplifyPrefix' [] _ = []
        simplifyPrefix' (x:xs) before
            | x == '/' && before == '/' = simplifyPrefix' xs x
            | otherwise = x : simplifyPrefix' xs x

-- parse a path into it's directories. The path is simplified before splitting
pathSegments :: String -> [String]
pathSegments path = pathSegments' (simplifyPrefix path) []
    where
        pathSegments' [] buf = [buf]
        pathSegments' (x:xs) buf
            | x == '/' = buf : pathSegments' xs []
            | otherwise = pathSegments' xs (buf ++ [x])

-- returns a base dir that is always valid, regardless whether the Maybe contained a value or not
-- The returned baseDir always ends on / or is empty.
baseDirOrDefault (Just baseDir) = baseDir ++ "/"
baseDirOrDefault Nothing        = ""

-- translated from https://github.com/woboq/qmetaobject-rs/blob/master/qmetaobject_impl/src/qrc_impl.rs#L144
qtHash :: String -> Int
qtHash = foldl (\hash char -> let h = (hash `shiftL` 4) + ord char
                              in ((((h .&. 0xf0000000) `shiftR` 23) `xor` h) .&. 0x0fffffff)) 0

hashString :: String -> HashedString
hashString key = HashedString {hash = qtHash key, string = key}

buildTree :: [Resource] -> TreeNode
buildTree [] = newDir
buildTree resources = 
    let files = processResources resources
        tree  = foldl buildPath newDir files
    in computeOffsets tree 1
    where
        -- Convert resources into list of tuples (virtual path, filesystem path)
        processResources :: [Resource] -> [(String, String)]
        processResources resources = concatMap (\res -> map (processFile res) $ files res) resources
            where
                processFile res (File path alias) =
                    let virtualPath = prefix res ++ fromMaybe path alias
                        diskPath    = baseDirOrDefault (baseDir res) ++ path
                    in (virtualPath, diskPath)

        buildPath tree (virtualPath, diskPath) = buildPath' (pathSegments virtualPath) diskPath tree
            where
                buildPath' [file] diskPath parentNode     = insertChild file (newFile diskPath) parentNode
                buildPath' (dir:path) diskPath parentNode = insertChild dir (buildPath' path diskPath newDir) parentNode
                buildPath' [] diskPath parentNode         = parentNode

toU32Be :: Word32 -> [Word8]
toU32Be val = let val1 = fromIntegral $ val `shiftR` 24 .&. 0xff
                  val2 = fromIntegral $ val `shiftR` 16 .&. 0xff
                  val3 = fromIntegral $ val `shiftR` 8  .&. 0xff
                  val4 = fromIntegral $ val .&. 0xff
              in  [val1, val2, val3, val4]

toU16Be :: Word16 -> [Word8]
toU16Be val = let val1 = fromIntegral $ val `shiftR` 8 .&. 0xff
                  val2 = fromIntegral $ val .&. 0xff
              in  [val1, val2]

--
-- Binary format
--

data Data = Data {
    payload :: [Word8],
    names :: [Word8],
    treeData :: [Word8],
    fileNames :: [String]
} deriving (Show)

insertFile :: Data -> String -> IO Data
insertFile (Data payload names treeData fileNames) fileName = do
    contents <- BS.readFile fileName
    let newPayload   = payload ++ toU32Be (fromIntegral $ BS.length contents)
                               ++ BS.unpack contents
        newFileNames = fileNames ++ [fileName]
    return (Data newPayload names treeData newFileNames)

insertName :: Data -> HashedString -> (Data, Word32)
insertName (Data payload names treeData fileNames) (HashedString hash string) = do
    let offset = fromIntegral $ length names
    let newNames = names ++ toU16Be (fromIntegral $ length string)
                         ++ toU32Be (fromIntegral hash)
                         ++ stringToWord8 string
    (Data payload newNames treeData fileNames, offset)
    where
        stringToWord8 chars = concatMap (toU16Be . fromIntegral . ord) chars

insertDirectory :: Data -> [(HashedString, TreeNode)] -> IO Data
insertDirectory d contents = do
    dat <- insertChildren d contents
    insertContents dat contents
    where
        insertTreeNode :: TreeNode -> Data -> IO Data
        insertTreeNode (FileNode filename) (Data payload names treeData fileNames) =
            let offset = length payload
                newTreeData = treeData ++ toU16Be 0               -- flags
                                       ++ toU16Be 0               -- country
                                       ++ toU16Be 1               -- lang (C)
                                       ++ toU32Be (fromIntegral offset)
            in  insertFile (Data payload names newTreeData fileNames) filename

        insertTreeNode (DirectoryNode contents offset) (Data payload names treeData fileNames) =
            let newTreeData    = treeData ++ toU16Be 2       -- directory flag
                                          ++ toU32Be (fromIntegral $ length contents)
                                          ++ toU32Be (fromIntegral offset)
            in  return (Data payload names newTreeData fileNames)

        insertChildren :: Data -> [(HashedString, TreeNode)] -> IO Data
        insertChildren d [] = return d
        insertChildren d ((hashedName, node):contents) = do
            let (Data payload names treeData fileNames, nameOff) = insertName d hashedName
            let dat = Data payload names (treeData ++ toU32Be nameOff) fileNames
            Data payload names treeData newFileNames <- insertTreeNode node dat

            -- mofification time (64-bit) FIXME
            let tdModTime = treeData ++ toU32Be 0 ++ toU32Be 0
            insertChildren (Data payload names tdModTime newFileNames) contents

        insertContents :: Data -> [(HashedString, TreeNode)] -> IO Data
        insertContents d ((_, DirectoryNode contents offset):xs) = do
            dat <- insertDirectory d contents
            insertContents dat xs
        insertContents d _ = return d

generateData :: TreeNode -> IO Data
generateData (DirectoryNode contents _) =
    let treeData = toU32Be 0                -- fake name
                ++ toU16Be 2                -- flag
                ++ toU32Be (fromIntegral $ length contents)
                ++ toU32Be 1
                ++ toU32Be 0 ++ toU32Be 0   -- mofification time (64-bit) FIXME
    in  insertDirectory (Data [] [] treeData []) contents
generateData _ = error "root needs to be a dir"

-- | Main entry point for generating resource data from resources
processQrc :: [Resource] -> IO Data
processQrc resources = generateData (buildTree resources)

showPretty :: (Show a) => a -> IO ()
showPretty = putStrLn . beautify . show
    where
        beautify [] = []
        beautify xs = beautify' xs 1
            where
                indent i = replicate (4 * i) ' '
                beautify' [] _ = []
                beautify' (x:xs) i
                    | x == ','             = x:' ': beautify' xs i
                    | x == '{' || x == '[' = x:'\n' : indent i ++ beautify' xs (i + 1)
                    | x == '}' || x == ']' = '\n' : indent (i - 2) ++ x:beautify' xs (i - 1)
                    | otherwise            = x : beautify' xs i

-- | Include resources into the application's executable.
--
-- The following code includes the file "main.qml" into the resource "/".
-- It will be accessible for QML at "qrc:/main.qml"
--
-- The template haskell code should be inserted into the function that starts the qml engine,
-- usually the main function, before starting the engine.
--
-- @
--     $(includeResources [
--          Resource Nothing "/" [
--              File "main.qml" Nothing
--          ]])
-- @
--
includeResources :: [Resource] -> Q Exp
includeResources resources = do
    debug <- runIO $ lookupEnv "HSQML_DEBUG_QRC"
    when (isJust debug) (printDebug resources)

    let allFiles = concatMap (\r -> map (normalizeFilePath (baseDir r) . path) (files r)) resources

    mapM_ qAddDependentFile allFiles

    Data payload names treeData fileNames <- runIO $ processQrc resources

    -- Variable and function names
    let payloadId  = mkName "payload"
    let namesId    = mkName "names"
    let treeDataId = mkName "treeData"
    let regResId   = mkName "Graphics.QML.registerResourceData"

    let registerBody = VarE regResId `AppE` LitE (IntegerL 2) `AppE` VarE treeDataId `AppE` VarE namesId `AppE` VarE payloadId

    -- Create the named values that contain the lists, e.g @ payload = unsafePackLenLiteral 8 "qrc_data"# @
    let payloadDecl = listValueDeclaration payloadId payload
    let namesDecl = listValueDeclaration namesId names
    let treeDataDecl = listValueDeclaration treeDataId treeData

    -- Create an @ let payload = [..] in registerResourceData .. @ expression
    let binding = LetE [payloadDecl, namesDecl, treeDataDecl] registerBody
    return binding

    where
        printDebug resources = do
            runIO $ putStrLn "Virtual file system tree:"
            let tree = buildTree resources
            runIO $ showPretty tree
            runIO $ putStrLn "Generated binary data:"
            dat <- runIO $ generateData tree
            runIO $ showPretty dat

        listValueDeclaration name list = ValD (VarP name) (NormalB (packByteStringExpression (listExpression list) (length list))) []
        listExpression xs = LitE (StringPrimL xs)
        packByteStringExpression listDecl len = VarE (mkName "Data.ByteString.Internal.unsafePackLenLiteral") `AppE` LitE (IntegerL $ fromIntegral len) `AppE` listDecl
        normalizeFilePath baseDir path = baseDirOrDefault baseDir ++ path

--
-- Runtime support code
--

-- | Registers the resource data in Qt Resource format
--
-- This function is used by the code generated by the 'includeResources' function.
-- There is no reason to use it directly, as it depends on the binary data
-- created by the Resources module.
registerResourceData :: Int -> ByteString -> ByteString -> ByteString -> IO Bool
registerResourceData format treeData names payload = do
    ret <- hsqmlRegisterResourceData format (dataPtr treeData) (dataPtr names) (dataPtr payload)
    return $ ret == 1
    where
        dataPtr bs =
            -- The use of unsafeForeignPtrToPtr is safe here, because the ptr points to static memory that can never be freed
            let (ptr, _, _) = toForeignPtr bs in unsafeForeignPtrToPtr (castForeignPtr ptr)
