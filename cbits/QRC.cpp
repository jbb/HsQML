// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: BSD-3-Clause

// The qRegisterResourceData function is not part of the public API, but it is exported for use by
// rcc generated code
bool qRegisterResourceData(int, const unsigned char *,
                           const unsigned char *, const unsigned char *);

extern "C" int hsqml_register_resource_data(int version, const unsigned char *tree,
                                             const unsigned char *name, const unsigned char *data) {
    return qRegisterResourceData(version, tree, name, data);
}
